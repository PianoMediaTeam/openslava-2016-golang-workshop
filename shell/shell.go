package shell

import (
	"bytes"
	"io"

	"bitbucket.org/PianoMediaTeam/openslava-2016-golang-workshop/compiler"
	"bitbucket.org/PianoMediaTeam/openslava-2016-golang-workshop/process/scheduler"
)

type Shell struct {
	Compiler  compiler.Compiler
	Scheduler scheduler.Scheduler
}

type shellProcess struct {
	runner compiler.Runner
	output io.Writer
}

func (p *shellProcess) SetOutput(output io.Writer) {
	p.output = output
}

func (p *shellProcess) Start() error {
	o, err := p.runner.Run()
	if err != nil {
		return err
	}
	_, err = io.Copy(p.output, bytes.NewBuffer(o))
	return err
}

func (s *Shell) Execute(code string) (<-chan io.Reader, error) {
	r, err := s.Compiler.Compile(code)
	if err != nil {
		return nil, err
	}

	p := &shellProcess{
		runner: r,
	}

	return s.Scheduler.Queue(p), nil
}
