.PHONY: help lint vet

help:
	@echo "Please use 'make <target>' where <target> is one of"
	@echo "      lint              to lint source code recursively"
	@echo "      vet               to vet source code recursively"

vet:
	@go vet $$(go list ./... | grep -vE '(/vendor|/talk)')

lint:
	@set -e; \
	for package in $$(go list ./... | grep -vE '(/vendor|/talk)'); \
	do golint -set_exit_status=true $$package; done
