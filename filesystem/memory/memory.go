package memory

import "bitbucket.org/PianoMediaTeam/openslava-2016-golang-workshop/filesystem"

// New returns Filesystem using Memory as backend.
func New() filesystem.Filesystem {
	return nil
}

// TODO: implement Filesystem interface using map[string][]byte and add mutex
// to protect this map to avoid race condition when used concurrently.
