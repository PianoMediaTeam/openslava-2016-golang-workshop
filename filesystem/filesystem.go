package filesystem

import "fmt"

// FileNotFoundError is error when expected file doesn't exist.
type FileNotFoundError string

func (e FileNotFoundError) Error() string {
	return fmt.Sprintf("file not found: %s", string(e))
}

// Filesystem provides basic funcionality to read and write file.
type Filesystem interface {
	// Read returns content of file with given filename.
	//
	// FileNotFoundError is returned when file doesn't exist.
	Read(filename string) ([]byte, error)
	// Write writes given data into file with given filename.
	// In case that file already exist, it is overwritten.
	Write(filename string, data []byte) error
}
