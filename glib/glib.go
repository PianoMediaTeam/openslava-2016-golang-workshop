package glib

import (
	"errors"

	"bitbucket.org/PianoMediaTeam/openslava-2016-golang-workshop/filesystem"
)

type Internal struct {
	Name  string
	Value interface{}
}

type Glib interface {
	Internals() []*Internal
}

func New(fs filesystem.Filesystem) Glib {
	return &glib{
		fs: fs,
	}
}

type glib struct {
	fs filesystem.Filesystem
}

func (g *glib) Internals() []*Internal {
	return []*Internal{
		{
			Name:  "echo",
			Value: echo,
		},
		{
			Name: "read",
			// Notice binding of read function to receiver g.
			Value: g.read,
		},
	}
}

// read accepts one string argument and returns []byte or error.
func (g *glib) read(args []interface{}) (interface{}, error) {
	if len(args) != 1 {
		return nil, errors.New("function read takes one argument")
	}
	fileName, ok := args[0].(string)
	if !ok {
		return nil, errors.New("string argument expected")
	}
	return g.fs.Read(fileName)
}

// echo accepts one string argument and returns error.
func echo(args []interface{}) (interface{}, error) {
	if len(args) != 1 {
		return nil, errors.New("function read takes one argument")
	}
	value, ok := args[0].(string)
	if !ok {
		return nil, errors.New("string argument expected")
	}
	return value, nil
}

// TODO: implement write method
// TODO: implement sleep method
// TODO: implement http method which will do http GET request to given URL
// and returns response body as string.
