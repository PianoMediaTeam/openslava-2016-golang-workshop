Go programming language workshop

Juraj Sottnik
Gopher at piano
juraj.sottnik@piano.io

Peter Dulacka
Gopher at piano
peter.dulacka@piano.io

* Agenda

- introduction to language
- introduction to tooling
- do something real

* Introduction to Go programming language

* Syntax

    package bus
 
    import "fmt"
 
    import . "fmt"
 
    const IRQ = 1
 
    var a, b int
 
    var a = 3.14
 
    func Read() error {
        return nil
    }
 
    err := errors.New("blee")
 
    for i := 0; i < 10; i++ {
 
    for i < 10 {
 
    for {

* Syntax

    if i < 10 {
 
    if err := Read(); err != nil { // err exists only in scope of if statement
 
    switch i := Something(); i {
        case 1:
 
    switch {
        case i > 5 && i < 10:
    }
 
    defer fmt.Println(time.Now()) // arguments are evaluated immediately
 
    type Point struct {
        X, Y int
    }
 
    p := Point{
        X: 1,
        Y: 2,
    }

* Syntax

    var buf [5]byte // size of array can not be changed
 
    values := []bool{true, true, false} // slice creates undelying array automatically
 
    values = append(values, false)
 
    values = values[1:2]
 
    values = values[1:]
 
    for i, v := range values {
 
    for _, v := range values {
 
    m := make(map[string]Point)
 
    p, ok := m["key1"]
 
    m["key2"] = Point{

* Syntax

    a := []byte("hello")
 
    a := b.(Point) // will panic if type conversion is not possible
 
    a, ok := b.(Point) // without panic, notice ok idiom like in case of map
 
    switch i := x.(type) {
    case int:
        i++ // type of i is int
    }
 
    m := make(map[int]string)
 
    c := make(chan Point)
 
    c := make(chan Point, 10)
 
    s := make([]int, 10, 100)
 
* Syntax

    p := &Point{
        X: 1,
    }
 
    p := new(Point) // same as previous but no way to specify fields of Point

* Functions

    Read(p []byte) (n int, err error) {
        ...
    }

Function can returns multiple return values. Ideal for propagating error
(no more `return`-1`).

    bytes, err := Read([]byte("hello world"))
    if err != nil {
        log.Fatalf("read error: %s", err)
    }

* Pointers

Everything in Go is passed by value ([[https://golang.org/doc/faq#pass_by_value]]).

.play intro/pointer.go /type Bus/,/END OMIT/

* Pointers

A pointer holds the memory address of a variable.

    var bus *Bus
    bus = &Bus{}

same as 

    var bus = &Bus{}

same as

    bus := &Bus{} // preferred

same as

    bus := new(Bus)

Variable `bus` is of type pointer to `Bus`.

* Methods

Just function with receiver.

    type Bus struct {
        Empty bool
    }

    func (b *Bus) Read(p []byte) (n int, err error) {
        ...
    }

    func (b *Bus) Write(p []byte) (n int, err error) {
        b.Empty = false
    }

Same as function

    Read(b *Bus, p []byte) (n int, err error) {
        ...
    }

Pointer receiver if we need to modify it within method.

* Interfaces

    type Reader interface {
        Read(p []byte) (n int, err error)
    }
    
    type Writer interface {
        Write(p []byte) (n int, err error)
    }
    
    type ReadWriter interface {
        Reader
        Writer
    }

`Bus` satisfies `ReadWriter` implicitly without need to declare it on `Bus`
explicitly.

We can introduce interface later based on existing implementation easily.

`guru` helps to find interfaces that are implemented by some type and set of concrete types that implement some interface.

* Goroutines

A goroutine is a lightweight thread managed by the Go runtime.

BAD

.play intro/go_closure_bad.go /START/,/END/

GOOD

.play intro/go_closure_good.go /START/,/END/

    go fmt.Println(time.Now()) // arguments are evaluated immediately

* Mutex

If something is accessed concurrently (by more gourutines) it must be protected by `Mutex`.

    type Counter struct {
        mux sync.RWMutex // idiom - protects following value
        value int
    }

    func (c *Counter) Add() {
        c.mux.Lock()
        c.value++
        c.mux.Unlock()
    }

    func (c *Counter) Value() int {
        c.mux.RLock()
        defer c.mux.RUnlock()
        return c.value
    }

* Channels

.play intro/channel.go /START/,/END/

WaitGroup 

.play intro/channel_wg.go /START/,/END/

* Packages

*bitbucket.org/PianoMediaTeam/openslava-2016-golang-workshop/process*
process.go

    package process

    // process related stuff

scheduler.go

    package process

    // scheduler related stuff

Build results into package object.

*bitbucket.org/PianoMediaTeam/openslava-2016-golang-workshop/cmd/kernel*
main.go

    package main

Build results into *kernel* binary.

* Introduction to tooling

* Tools

- goimport (gofmt)
- guru
- eg
- golint
- go generate
- go vet
- go test
- gometalinter
- playground

gometalinter integrates many other tools for statically checking Go source code.
For example dead code, cyclomatic complexity, error checking, duplicity of code
and other.

* Documentation

- godoc
- Tour
- Effective Go
- FAQ
- Blog

* Vendoring

Part of Go from 1.6.

Source code of third party libraries are part of repository placed in *vendor* folder.

There are tools like [[https://github.com/tools/godep][godep]] to manage vendor folder for you.

* Code time

* Very simplified kernel

- `Shell` is initialized with access to `Compiler` and `Scheduler`.
- `Services` are registered into initd.
- `Initd` is started and it starts all registered `Services` with `Shell` provided.

Shell

Provides functionality to transform given code to `Runner` and transform `Runner` to `Process` which is finally send to `Scheduler`.

Compiler

We are using simple extendible language - [[http://blog.labix.org/2013/07/16/twik-a-tiny-language-for-go][twik]].

Scheduler

Simple implementation to run unlimited number of `Processes`.

Services

Terminal console to interact with `Shell`.

* Where to Go

[[https://golang.org/doc/]]
