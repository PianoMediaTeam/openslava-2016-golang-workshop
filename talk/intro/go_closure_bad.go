package main

import (
	"fmt"
	"time"
)

func main() {
	// START OMIT
	for i := 0; i < 5; i++ {
		go func() { // process every value in own goroutine
			time.Sleep(time.Second) // simulate some work
			fmt.Println(i)          // all closures see same variable
		}()
	}
	// END OMIT
	time.Sleep(2 * time.Second)
}
