package main

import (
	"fmt"
	"sync"
	"time"
)

func main() {
	// START OMIT
	var wg sync.WaitGroup
	for i := 0; i < 5; i++ {
		wg.Add(1)
		go func(i int) {
			time.Sleep(time.Duration(i*500) * time.Millisecond)
			fmt.Println(i)
			wg.Done()
		}(i)
	}
	wg.Wait()
	// END OMIT
}
