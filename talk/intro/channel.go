package main

import "fmt"

func main() {
	// START OMIT
	workers := 5
	c := make(chan int)
	for i := 0; i < workers; i++ {
		go func(i int) {
			c <- i
		}(i)
	}
	for i := 0; i < workers; i++ {
		fmt.Println(<-c)
	}
	// END OMIT
}
