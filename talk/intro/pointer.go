package main

import "fmt"

type Bus struct {
	Size int
}

func Write1(b Bus) {
	b.Size = 11 // change is not propagated to b, because it is done on copy of b
}

func Write2(b *Bus) {
	b.Size = 11 // change is propagated
}

func main() {
	b := Bus{
		Size: 1,
	}
	Write1(b)
	fmt.Println(b.Size)
	Write2(&b)
	fmt.Println(b.Size)
}

// END OMIT
