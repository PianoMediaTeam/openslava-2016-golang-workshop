package initd

import (
	"fmt"
	"io"
	"sync"
)

type Service interface {
	ID() string
	Run() error
}

type Initd struct {
	services []Service
}

func (initd *Initd) Register(s Service) {
	initd.services = append(initd.services, s)
}

func (initd *Initd) Run(output io.Writer) {
	l := len(initd.services)
	var wg sync.WaitGroup
	wg.Add(l)
	for _, s := range initd.services {
		go func(s Service) {
			err := s.Run()
			if err != nil {
				fmt.Fprintf(output, "service [%s] error: %s", s.ID(), err)
			}
			wg.Done()
		}(s)
	}
	wg.Wait()
}
