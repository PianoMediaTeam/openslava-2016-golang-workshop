# Go programming language workshop

## Speaker

### Prepare

    go get golang.org/x/tools/cmd/present

### Talk

Talk is written using [present tool](https://godoc.org/golang.org/x/tools/cmd/present)
which provides functionality to define slides of talk in plain text file using specific
formatting syntax and allows to insert go source code into slide with possibility to execute
it directly. So we can have talk slides versioned together with go source code used in
slides.

#### How to write

See [doc of present package](https://godoc.org/golang.org/x/tools/present) to learn syntax.

See [other talks](https://github.com/golang/talks) to see real world usage.

Sources of talk are placed in `talk` folder.

#### How to present

See [doc of present command](https://godoc.org/golang.org/x/tools/cmd/present) to learn
how to run presentation.

## Participant

### Prerequisites

- Go 1.8.x [installed](https://golang.org/doc/install) on local machine and [verified](https://golang.org/doc/install#testing) that it is installed correctly (also with GOPATH environment variable)
- [git](https://git-scm.com/) tool installed
- complete [how to write Go code](https://golang.org/doc/code.html) tutorial (encountered issues can be discussed on workshop)
- favorite [editor with Go support](https://github.com/golang/go/wiki/IDEsAndTextEditorPlugins)
    installed
- [Go Guru tool](http://golang.org/s/using-guru) installed together with plugin for your favorite editor (not mandatory)
- ideally [Go tour](https://tour.golang.org) completed (will be summarized on workshop too)

### Other tools

    go get golang.org/x/tools/cmd/goimports

    go get github.com/golang/lint/golint

    go get github.com/rogpeppe/godef

### Workshop source code

    go get bitbucket.org/PianoMediaTeam/openslava-2016-golang-workshop/cmd/kernel
