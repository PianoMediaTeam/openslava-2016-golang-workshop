package main

import (
	"os"

	"bitbucket.org/PianoMediaTeam/openslava-2016-golang-workshop/compiler"
	"bitbucket.org/PianoMediaTeam/openslava-2016-golang-workshop/filesystem/memory"
	"bitbucket.org/PianoMediaTeam/openslava-2016-golang-workshop/glib"
	"bitbucket.org/PianoMediaTeam/openslava-2016-golang-workshop/initd"
	"bitbucket.org/PianoMediaTeam/openslava-2016-golang-workshop/process/scheduler"
	"bitbucket.org/PianoMediaTeam/openslava-2016-golang-workshop/shell"
	"bitbucket.org/PianoMediaTeam/openslava-2016-golang-workshop/terminal/console"
)

func main() {
	shell := shell.Shell{
		Compiler:  compiler.New(glib.New(memory.New())),
		Scheduler: &scheduler.UnlimitedScheduler{},
	}

	initd := initd.Initd{}
	initd.Register(&console.Console{
		Shell: shell,
	})
	initd.Run(os.Stdout)
}
