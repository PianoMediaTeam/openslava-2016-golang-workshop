// Package compiler provides pseudo compiler of Twik programming language
// into runnable. As mentioned this this is implementation of pseudo compiler
// because Twik is interpreted.
//
// See https://blog.labix.org/2013/07/16/twik-a-tiny-language-for-go for
// details of Twik programming language.
package compiler

import (
	"fmt"

	"bitbucket.org/PianoMediaTeam/openslava-2016-golang-workshop/glib"
	"gopkg.in/twik.v1"
	"gopkg.in/twik.v1/ast"
)

type Runner interface {
	Run() ([]byte, error)
}

type Compiler interface {
	Compile(code string) (Runner, error)
}

func New(glib glib.Glib) Compiler {
	return &compiler{
		glib: glib,
	}
}

type compiler struct {
	glib glib.Glib
}

// Compilee tries to compile given code into Runner.
func (c *compiler) Compile(code string) (Runner, error) {
	fset := twik.NewFileSet()

	node, err := twik.ParseString(fset, "internal", code)
	if err != nil {
		return nil, err
	}

	scope := twik.NewScope(fset)
	for _, internal := range c.glib.Internals() {
		scope.Create(internal.Name, internal.Value)
	}

	return &runner{
		node:  node,
		scope: scope,
	}, nil
}

type runner struct {
	node  ast.Node
	scope *twik.Scope
}

func (b *runner) Run() ([]byte, error) {
	o, err := b.scope.Eval(b.node)
	if err != nil {
		return nil, err
	}
	return []byte(fmt.Sprintf("%v", o)), nil
}
