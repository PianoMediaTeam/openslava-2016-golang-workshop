package console

import (
	"bufio"
	"fmt"
	"io"
	"os"

	"bitbucket.org/PianoMediaTeam/openslava-2016-golang-workshop/shell"
)

type Console struct {
	Shell shell.Shell
}

func (c *Console) ID() string {
	return "Console"
}

func (c *Console) Run() error {
	prompt := "> "
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Print(prompt)
	for scanner.Scan() {
		input := scanner.Text()
		c, err := c.Shell.Execute(input)
		if err != nil {
			fmt.Fprintf(os.Stdout, "error: %s\n", err)
			continue
		}

		o := <-c

		io.Copy(os.Stdout, o)
		fmt.Println("")
		fmt.Print(prompt)

	}

	if err := scanner.Err(); err != nil {
		return err
	}

	return nil
}
