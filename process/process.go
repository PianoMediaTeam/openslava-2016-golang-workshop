package process

import "io"

type Process interface {
	// SetOuput sets output which process must use.
	SetOutput(output io.Writer)
	// Start starts process.
	Start() error
}
