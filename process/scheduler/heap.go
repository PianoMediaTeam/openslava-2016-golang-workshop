package scheduler

import "container/heap"

type processHeap struct {
	items []*runProcess
}

func (ph *processHeap) Init() {
	heap.Init(ph)
}

func (ph *processHeap) IsEmpty() bool {
	return len(ph.items) == 0
}

func (ph *processHeap) Push(i interface{}) {
	ph.items = append(ph.items, i.(*runProcess))
}

func (ph *processHeap) Pop() interface{} {
	i := ph.items[0]
	ph.items = ph.items[1:]
	return i
}

func (ph processHeap) Len() int {
	return len(ph.items)
}

func (ph *processHeap) Less(i, j int) bool {
	return i < j
}

func (ph *processHeap) Swap(i, j int) {
	ph.items[i], ph.items[j] = ph.items[j], ph.items[i]
	return
}
