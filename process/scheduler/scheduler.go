package scheduler

import (
	"io"

	"bitbucket.org/PianoMediaTeam/openslava-2016-golang-workshop/process"
)

type Scheduler interface {
	// Queue queues give Process and returns channel which returns Reader to
	// read output of process when process is done.
	//
	// Process.SetOutput is always called before Process.Start.
	Queue(process.Process) <-chan io.Reader
}
