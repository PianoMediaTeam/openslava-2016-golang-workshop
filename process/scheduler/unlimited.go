package scheduler

import (
	"bytes"
	"fmt"
	"io"

	"bitbucket.org/PianoMediaTeam/openslava-2016-golang-workshop/process"
)

type UnlimitedScheduler struct{}

func (s *UnlimitedScheduler) Queue(p process.Process) <-chan io.Reader {
	c := make(chan io.Reader)
	var buf bytes.Buffer
	p.SetOutput(&buf)
	go func() {
		err := p.Start()
		if err != nil {
			fmt.Fprintf(&buf, "error occurred: %s", err)
		}
		c <- &buf
	}()
	return c
}
