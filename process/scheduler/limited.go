package scheduler

import (
	"bytes"
	"fmt"
	"io"
	"time"

	"bitbucket.org/PianoMediaTeam/openslava-2016-golang-workshop/process"
)

// LimitedScheduler implements Scheduler interface. Zero value is unusable.
type LimitedScheduler struct {
	queue *processHeap
}

type runProcess struct {
	process.Process
	fn func()
}

func NewLimitedScheduler(maxSize int) *LimitedScheduler {
	queue := &processHeap{}
	queue.Init()
	runProcs := make(chan func(), maxSize)

	ls := LimitedScheduler{
		queue: queue,
	}

	go func() {
		for {
			if queue.Len() == 0 {
				time.Sleep(time.Millisecond * 100)
				continue
			}
			p := queue.Pop().(*runProcess)
			runProcs <- p.fn
		}
	}()

	go func() {
		for proc := range runProcs {
			go proc()
		}
	}()

	return &ls
}

func (ls *LimitedScheduler) Queue(p process.Process) <-chan io.Reader {
	c := make(chan io.Reader)
	var buf bytes.Buffer
	p.SetOutput(&buf)

	run := func() {
		err := p.Start()
		if err != nil {
			fmt.Fprintf(&buf, "error occurred: %s", err)
		}
		c <- &buf
	}
	ls.queue.Push(&runProcess{
		Process: p,
		fn:      run,
	})

	return c
}
